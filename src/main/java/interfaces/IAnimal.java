package interfaces;

import java.util.List;

import entity.Animal;

public interface IAnimal {
    List<Animal> findAll();

    Animal findById(Integer id);

    boolean save(Animal animal);

    boolean update(Animal animal);

    boolean deleteById(Integer id);
}
