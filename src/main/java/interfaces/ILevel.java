package interfaces;

import java.util.List;

import entity.Level;

public interface ILevel {
    List<Level> findAll();

    Level findById(Integer id);

    boolean save(Level level);

    boolean update(Level level);

    boolean deleteById(Integer id);
}