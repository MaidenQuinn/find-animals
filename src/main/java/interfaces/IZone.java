package interfaces;

import java.util.List;

import entity.Zone;

public interface IZone {
    List<Zone> findAll();

    Zone findById(Integer id);

    boolean save(Zone zone);

    boolean update(Zone zone);

    boolean deleteById(Integer id);
}