package interfaces;

import java.util.List;

import entity.User;

public interface IUser {
    List<User> findAll();

    User findById(Integer id);

    boolean save(User user);

    boolean update(User user);

    boolean deleteById(Integer id);
}