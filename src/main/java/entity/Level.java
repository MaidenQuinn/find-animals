package entity;

public class Level {
    private Integer idLevel;
    private int number;
    private int nbOfAnimals;
    private boolean completed;

    public Level(Integer idLevel, int number, int nbOfAnimals, boolean completed) {
        this.idLevel = idLevel;
        this.number = number;
        this.nbOfAnimals = nbOfAnimals;
        this.completed = completed;
    }

    public Level(Integer idLevel) {
        this.idLevel = idLevel;
    }

    public Level() {
    }

    public Integer getIdLevel() {
        return idLevel;
    }

    public void setIdLevel(Integer idLevel) {
        this.idLevel = idLevel;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNbOfAnimals() {
        return nbOfAnimals;
    }

    public void setNbOfAnimals(int nbOfAnimals) {
        this.nbOfAnimals = nbOfAnimals;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
