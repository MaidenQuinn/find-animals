package entity;

public class User {
    private Integer idUser;
    private String pseudo;
    private String password;
    private String role;

    public User(Integer idUser, String pseudo, String password, String role) {
        this.idUser = idUser;
        this.pseudo = pseudo;
        this.password = password;
        this.role = role;
    }

    public User(Integer idUser) {
        this.idUser = idUser;
    }

    public User() {
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
