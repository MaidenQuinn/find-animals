package entity;

public class Zone {
    private Integer idZone;
    private String name;

    public Zone(Integer idZone, String name) {
        this.idZone = idZone;
        this.name = name;
    }

    public Zone(Integer idZone) {
        this.idZone = idZone;
    }

    public Zone() {
    }

    public Integer getIdZone() {
        return idZone;
    }

    public void setIdZone(Integer idZone) {
        this.idZone = idZone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
