package entity;

public class Animal {
    private Integer animalId;
    private String species;
    private String breed;
    private String color;
    private int height;
    private int weight;
    private int age;
    private boolean founded;

    public Animal(Integer animalId, String species, String breed, String color, int height, int weight, int age,
            boolean founded) {
        this.animalId = animalId;
        this.species = species;
        this.breed = breed;
        this.color = color;
        this.height = height;
        this.weight = weight;
        this.age = age;
        this.founded = founded;
    }

    public Animal(Integer animalId) {
        this.animalId = animalId;
    }

    public Animal() {
    }

    public Integer getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Integer animalId) {
        this.animalId = animalId;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isFounded() {
        return founded;
    }

    public void setFounded(boolean founded) {
        this.founded = founded;
    }
}
