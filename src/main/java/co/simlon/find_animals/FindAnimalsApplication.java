package co.simlon.find_animals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindAnimalsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindAnimalsApplication.class, args);
	}

}
