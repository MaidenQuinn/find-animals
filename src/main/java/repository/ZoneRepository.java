package repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.datasource.DataSourceUtils;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import entity.Zone;
import interfaces.IZone;

public class ZoneRepository implements IZone {

    @Autowired
    private DataSource dataSource;
    private Connection connection;

    @Override
    public List<Zone> findAll() {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("SELECT * FROM zone");
            ResultSet result = stmt.executeQuery();
            List<Zone> zoneList = new ArrayList<>();
            while (result.next()) {
                Zone zone = new Zone(
                        result.getInt("idZone"),
                        result.getString("name"));
                zoneList.add(zone);
            }
            return zoneList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    @Override
    public Zone findById(Integer id) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("SELECT * FROM zone WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Zone(
                        result.getInt("id"),
                        result.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    @Override
    public boolean save(Zone zone) {
        try {
            if (zone.getIdZone() != null) {
                return update(zone);
            }
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("INSERT INTO zone name VALUES ?",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, zone.getName());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                zone.setIdZone(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;

    }

    @Override
    public boolean update(Zone zone) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("UPDATE zone SET name=? WHERE id=?");
            stmt.setString(1, zone.getName());
            stmt.setInt(2, zone.getIdZone());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("DELETE FROM zone WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;

    }

}
